// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "Templates/SubclassOf.h"
#include "TablaDatos.h"
#include "Engine/DataTable.h"
#include "GameFramework/PlayerController.h"
#include "TopDownBase50PlayerController.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnHitAll);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnHability1);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnHability2);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnLevelUp);

class UNiagaraSystem;

UCLASS()
class ATopDownBase50PlayerController : public APlayerController
{
	GENERATED_BODY()

public:
	ATopDownBase50PlayerController();

	UPROPERTY(BlueprintAssignable)
	FOnHitAll evOnHitAll;
	UPROPERTY(BlueprintAssignable)
	FOnHability1 evOnHability1;
	UPROPERTY(BlueprintAssignable)
	FOnHability2 evOnHability2;
	UPROPERTY(BlueprintAssignable)
	FOnLevelUp eVOnLevelUp;

	UPROPERTY(BlueprintReadWrite, BlueprintReadWrite, Category=DATATABLES)
	UDataTable* ClassDB;
	UPROPERTY(BlueprintReadWrite, BlueprintReadWrite)
	FName className;
	UPROPERTY(BlueprintReadWrite, BlueprintReadWrite, Category=DATATABLES)
    UDataTable* SkillsDB;
    UPROPERTY(BlueprintReadWrite, BlueprintReadWrite)
    FName skillsName;

	FTablaDatos* miClass;
	FSkills* miSkills;

	virtual void BeginPlay() override;
	virtual void PlayerTick(float DeltaTime) override;
	void HitAll();
	void LevelUp();
	void GetClass();
	virtual void SetupInputComponent() override;
	void Hability1();
	void Hability2();

	/** Time Threshold to know if it was a short press */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input)
	float ShortPressThreshold;

	/** FX Class that we will spawn when clicking */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Input)
	UNiagaraSystem* FXCursor;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int XSpeed;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int lvl;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int XpMAX;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int XpACT;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	bool run;
	
protected:
	/** True if the controlled character should navigate to the mouse cursor. */
	uint32 bMoveToMouseCursor : 1;

	// Begin PlayerController interface
	//virtual void PlayerTick(float DeltaTime) override;
	//virtual void SetupInputComponent() override;
	// End PlayerController interface

	/** Input handlers for SetDestination action. */
	void OnSetDestinationPressed();
	void OnSetDestinationReleased();
	void OnTouchPressed(const ETouchIndex::Type FingerIndex, const FVector Location);
	void OnTouchReleased(const ETouchIndex::Type FingerIndex, const FVector Location);

private:
	bool bInputPressed; // Input is bring pressed
	bool bIsTouch; // Is it a touch device
	float FollowTime; // For how long it has been pressed
};


