// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataTable.h"
#include "Skills.generated.h"

UENUM(BlueprintType)
enum ESkillsName{
	FIREBALL,
	METEOR
};
/**
 * 
 */
USTRUCT(BlueprintType)
struct TOPDOWNBASE50_API FSkills : public FTableRowBase
{
	GENERATED_BODY()
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	TEnumAsByte<ESkillsName> skillname;
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	int dmg;
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	int dmgscale;
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	int range;

	FSkills() : skillname(FIREBALL), dmg(100), dmgscale(10), range(200){}
};
