// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Skills.h"
#include "Engine/DataTable.h"
#include "TablaDatos.generated.h"

UENUM(BlueprintType)
enum EClases {
	MAGO,
	GUERRERO
};
/**
 * 
 */
USTRUCT(BlueprintType)
struct TOPDOWNBASE50_API FTablaDatos : public FTableRowBase
{
	
	GENERATED_BODY()
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	TEnumAsByte<EClases> myclass;
	
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	int initialHp;
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	int initialMp;
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	TEnumAsByte<ESkillsName> hability1;
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	TEnumAsByte<ESkillsName> hability2;
	FTablaDatos() : myclass(MAGO), initialHp(50), initialMp(0), hability1(FIREBALL), hability2(METEOR)  {}
};
