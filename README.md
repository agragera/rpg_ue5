# TopDownBase50

**Risketos Basics**

- Sistema preparat per tenir diverses clases (Solament esta configurada per al mag) amb vida, manà, dany, rang, multiplicador dany.
- Tinc 2 habilitats (amb les tecles 'Q' i 'W'), la primera es un projectil ràpid que no fa molt de mal i el segón es mes petit pero es mes lent
- He preparat el joc per poder rebre mal pero no he pogut fer que els enemics ataquin. (Ho he comprovat llençant habilitats yo mateix que em fessin mal a mi mateix)
- Guanyem experiència a mesura que anem matant enemics, que ens donarán punts d'experiència, el que ens fará pujar de nivell guanyant així mes vida máxima (es podria ficar una millora de dany o altra cosa amb els multiplicadors de dany que ya estan implementats)

**Risketos Opcionals**

- sistema d'animacions avançat amb BlendTrees
